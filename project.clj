(defproject clopher "0.1.0-SNAPSHOT"
  :description "Console Gopher client"
  :url "https://gitlab.com/ElenQ/clopher"
  :license {:name "GPL 3.0"
            :url "https://www.gnu.org/licenses/gpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [net.java.dev.jna/jna "5.2.0"]]
  :repl-options {:init-ns clopher.core
                 :port 7000}
  :profiles {:uberjar {:aot :all}}
  :main ^:skip-aot clopher.core
  :source-paths ["src/clojure"]
  :java-source-paths ["src/java"]
  :javac-options     ["-Xlint:unchecked"]
  )
