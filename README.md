# Clopher

Clopher (still under development) is a terminal Gopher client written in
Clojure (and some Java because I was forced to do it). The goal of the project,
further than the creation of a Gopher client, is to serve as a tool to learn
all the middle points needed to create this application from scratch.

Consider it a yak shaving exercise that might led to the creation of new code
that might form a library in the future.

It is developed with a no-dependency philosophy but JNA (Java Native Interface)
was added in order to make the TUI work correctly.

## Portability

Clopher is developed with GNU/Linux in mind but it should work on any POSIX
operating system.

At the moment Clopher doesn't target non-POSIX compatible Operating Systems.
Clopher makes use of POSIX Terminal Interface for the TUI and that limits the
Operating Systems where it can run.

There's no plan to make it work on other platforms but contributions are
welcome.

## License

    Copyright (C) 2019 Ekaitz Zárraga <ekaitz@elenq.tech>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

