package clopher.termios;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;


/**
 * Interface to Posix libc
 */
public class Termios extends Structure {
    private int NCCS = 32;

    public int c_iflag;           // input mode flags
    public int c_oflag;           // output mode flags
    public int c_cflag;           // control mode flags
    public int c_lflag;           // local mode flags
    public byte c_line;           // line discipline
    public byte c_cc[];           // control characters
    public int c_ispeed;          // input speed
    public int c_ospeed;          // output speed

    public Termios() {
        c_cc = new byte[NCCS];
    }

    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "c_iflag",
                "c_oflag",
                "c_cflag",
                "c_lflag",
                "c_line",
                "c_cc",
                "c_ispeed",
                "c_ospeed"
                );
    }

    @Override
    public String toString() {
        return "termios{" +
            "c_iflag=" + c_iflag +
            ", c_oflag=" + c_oflag +
            ", c_cflag=" + c_cflag +
            ", c_lflag=" + c_lflag +
            ", c_line=" + c_line +
            ", c_cc=" + Arrays.toString(c_cc) +
            ", c_ispeed=" + c_ispeed +
            ", c_ospeed=" + c_ospeed +
            '}';
    }
}
