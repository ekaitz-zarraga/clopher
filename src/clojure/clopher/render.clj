(ns clopher.render
  (:require [clojure.string :as string]
            [clopher.ui :as ui]))

(defn abbr
  "Abbreviates a string to `max-size` taking in account `cursor-col` as the
  position of a single-character cursor in the string.
  Returns a shortened string of size `max-size` where the cursor is located."
  [string max-size cursor-col]
  (let [part (int (/ cursor-col max-size))]
      (->> string
           (drop (* max-size part))
           (take max-size)
           (apply str))))

(comment (abbr "Ramón y Cajal fue un" 10 11) ; "jal fue un"
         (abbr "Ramón y Cajal fue un" 10 3)) ; "Ramón y Ca"

(defn fill
  [string size]
  "Fills the string to the chosen size with spaces. If string fills the space
  or is larger returns it without changes."
  (let [strlen (count string)]
    (if (< strlen size)
      (apply str string (repeat (- size strlen) \space))
      string)))

(comment (fill "hola" 20)) ; "hola                "



(defn render-content
  "WIP: Testing content rendering.
  - Make sure non printable characters are filtered and newlines don't
  appear... They can break all the rendering."
  [content size cursor]
  (when (some #(< 0 %) (vals size))
    (let [x-size (:x size)
          y-size (:y size)
          x-cursor (:x cursor)
          y-cursor (:y cursor)]
      (->> content
         string/split-lines
         (map #(abbr % x-size x-cursor))
         (map #(fill % x-size))
         (map-indexed #(if (= %1 y-cursor)
                         (ui/wrap-color %2 :bg :blue)
                         %2))
         (drop (* y-size (int (/ y-cursor y-size))))
         (take y-size)
         (#(concat % (repeat (- y-size (count %)) "~")))))))
