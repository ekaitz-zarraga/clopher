(ns clopher.term
  (:import [clopher.termios Termios]
           [com.sun.jna Function]))

; This module makes easy to activate the Noncanonical mode and going back to it
; It might be extended to support other features in the future.
; More info:
; https://en.wikipedia.org/wiki/POSIX_terminal_interface#Input_processing

; Stolen from libc's definitions
; TODO make sure they work in non x64 architectures
(def ^:private ICANON 02)
(def ^:private ECHO   010)
(def ^:private ISIG   01)
(def ^:private ECHONL 0100)
(def ^:private IEXTEN 0100000)

(def ^:private VTIME 5)
(def ^:private VMIN  6)

; Stolen from:
; https://nakkaya.com/2009/11/16/java-native-access-from-clojure/
(defmacro jna-call [lib func ret & args]
  `(let [library#  (name ~lib)
         function# (Function/getFunction library# ~func)]
     (.invoke function# ~ret (to-array [~@args]))))

(defn get-config!
  []
  (let [term-conf (Termios.)]
    (if (= 0 (jna-call :c "tcgetattr" Integer 0 term-conf))
      term-conf
      (throw (UnsupportedOperationException.
               "Impossible to get terminal configuration")))))

(defn set-config!
  [term-conf]
  (when (not= 0 (jna-call :c "tcsetattr" Integer 0 0 term-conf))
    (throw (UnsupportedOperationException.
             "Impossible to set terminal configuration"))))

(defn set-non-canonical!
  "Sets noncanonical mode, makes (read *in*) act differently:
  - Doesn't buffer until a \n arrives.
  - If blocking is true (default) it waits until a key is pressed, if it's
  false it returns automatically with a 0 (POSIX also permits to return with
  -1)."
  ([]
   (set-non-canonical! true))
  ([blocking]
  (let [term-conf (get-config!)]
    (set! (.-c_lflag term-conf)
          (bit-and (.-c_lflag term-conf)
                   (bit-not (bit-or ICANON ECHO ISIG ECHONL IEXTEN))))
    (aset-byte (.-c_cc term-conf) VMIN  (if blocking 1 0))
    (aset-byte (.-c_cc term-conf) VTIME 0)
    (set-config! term-conf))))

(declare default-config)
(defn load-default-config!
  []
  (def default-config (get-config!)))

(defn restore-default-config!
  []
  (if (some? default-config)
    (set-config! default-config)
    (throw (UnsupportedOperationException.
             "Default configuration not loaded"))))
