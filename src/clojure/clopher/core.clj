(ns clopher.core
  (:require [clopher.net :as net]
            [clopher.definitions :as defs]
            [clopher.ui :as ui]
            [clopher.term :as term]
            [clopher.render :as render]
            [clojure.string :as string])
  (:gen-class))

(defn ->state
  "Create the initial state"
  []
  {
   :content "
            ,--,                         ,---,
          ,--.'|             ,-.----.  ,--.' |
          |  | :      ,---.  \\    /  \\ |  |  :                __  ,-.
          :  : '     '   ,'\\ |   :    |:  :  :              ,' ,'/ /|
   ,---.  |  ' |    /   /   ||   | .\\ ::  |  |,--.   ,---.  '  | |' |
  /     \\ '  | |   .   ; ,. :.   : |: ||  :  '   |  /     \\ |  |   ,'
 /    / ' |  | :   '   | |: :|   |  \\ :|  |   /' : /    /  |'  :  /
.    ' /  '  : |__ '   | .; :|   : .  |'  :  | | |.    ' / ||  | '
'   ; :__ |  | '.'||   :    |:     |`-'|  |  ' | :'   ;   /|;  : |
'   | '.'|;  :    ; \\   \\  / :   : :   |  :  :_:,''   |  / ||  , ;
|   :    :|  ,   /   `----'  |   | :   |  | ,'    |   :    | ---'
 \\   \\  /  ---`-'            `---'.|   `--''       \\   \\  /
  `----'                       `---`                `----'


Welcome to Clopher.

  - Start navigating enter: `:[URL]`
  - Or exit with: `:quit` "

   :content.type :text

   :content.cursor {:x 0
                    :y 0}
   :command ""
   :command.cursor 0 ; it's only one dimension (x)

   :screen-size {:x 0
                 :y 0}

   :location nil
   :history nil

   :console-report nil
   })



(defn console-report? ; TODO arrow keys and all that are harder to process!
  [report]
  (= (first "\033") (first report)))

(defn move-content-cursor
  "Moves the virtual cursor of the content view."
  ; TODO: Sticky in the Max and 0
  [state axis func]
  (if (some #(= 0 %) (vals (:screen-size state)))
    state
    (let [newpos        (update (:content.cursor state) axis func)
          contentlines  (string/split-lines (:content state))]
      (if (and (<= 0 (:x newpos) (dec (apply max (map count contentlines))))
               (<= 0 (:y newpos) (dec (count contentlines))))
        (assoc state :content.cursor newpos)
        state))))

(defn process-input
  "Called when a key is received, processes the input and updates the state
  with it. Returns the new state."
  [state k]
  (cond ; Be careful with the priority here!

    (= \: k)
      (assoc state :command (str k))

    (not= 0 (count (:command state))) ; User is inserting a command
    (if (= k \newline) ; Run command
      (do
        ; TODO process the command
        (assoc state :command ""))
      (assoc state :command (str (:command state) k)))

    ; Console reports
    ; Move to a :command style parsing
    (console-report? (str (:console-report state) k))
    (let [report (str (:console-report state) k)]
      ; TODO: check other kind of reports
      (if-let [pos (ui/parse-size-report report)]
          ; Report is formed, update screen-size
          (-> state
              (assoc :screen-size pos)
              (assoc :console-report nil))
          ; Report not formed yet
          (assoc state :console-report report)))

    (= \newline k) ;TODO navigate to link under cursor
      state

    (= \j k)
        (move-content-cursor state :y inc)

    (= \k k)
        (move-content-cursor state :y dec)

    (= \l k)
        (move-content-cursor state :x (partial + (:x (:screen-size state))))

    (= \h k)
        (move-content-cursor state :x (partial + (- (:x (:screen-size state)))))

    ; TODO remove this... it's here for debug
    :default (assoc state :current-key k)))

(defn render!
  [state]
  ; TODO make a double buffer
  (ui/move-cursor! 0 0)
  (ui/erase-screen!)
  ; TODO: Screen resizes destroy the scroll on the terminal! How to solve that?
  ; I think it's related with the 0,0 position. When the screen size changes
  ; the 0 position is in a different location maybe.
  (let [size (:screen-size state)]
    (dorun
      (map println (render/render-content
                     (:content state)
                     {:x (:x size) :y (- (:y size) 2)}
                     (:content.cursor state)))))
  (println (:screen-size state)
           (map byte (:console-report state)))
  (print (:command state))
  (flush))

(defn on-exit!
  "Cleanup function probably for side-effects. Return value is ignored."
  [state]
  (ui/move-cursor! 0 0)
  (ui/erase-screen-to-end!)
  (println "\nExiting..."))

(defn exit?
  "Decide if we need to exit or not. Return true or false."
  [state]
  (= \q (:current-key state)))


(defn -main
  [& args]
  (term/load-default-config!)
  (term/set-non-canonical!)
  (let [tick (future (while true
                       (ui/query-size!) ; Poll the screen size, and use it
                                        ; to force a state check -> tick
                       (Thread/sleep 1000)))]

    (loop [ch    (char (.read *in*))
           state (->state)]

      (let [newstate (process-input state ch)]

        (render! newstate)

        (if (exit? newstate)
          (do
            (on-exit! state)
            (future-cancel tick)
            (shutdown-agents)
            (term/restore-default-config!))

          (recur (char (.read *in*))
                 newstate))))))
