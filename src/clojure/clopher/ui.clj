(ns clopher.ui)

(comment
  ; All this sequences are referenced in lanterna as
  (byte 0x1b) ; 27 -> same as \033
  (byte \[)   ; 91 -> same as [
  ; https://github.com/mabe02/lanterna/blob/master/src/main/java/com/googlecode/lanterna/terminal/ansi/ANSITerminal.java
  ;
  ; More info here:
  ; https://en.wikipedia.org/wiki/ANSI_escape_code
  ;
  ; Or in man pages:
  ; man console_codes
  ;
  ; More info:
  ; https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
  )

(def ^:private csi-lead "\033[")
(defn str-csi
  [& args]
  (apply str csi-lead args))
(defn csi-command!
  [& args]
  (print (apply str-csi args))
  (flush))

; Move cursor
;------------------------------------------------------------------------------
(defn move-cursor!
  "Also valid with CSI<L>;<C>f being <L> and <C> line number and column
  number respectively"
  [x y]
  (csi-command! y ";" x "H"))

(defn move-cursor-delta-x!
  [x]
  (let [abs  (Math/abs x)
        comm (if (< 0 x) \C \D)]
  (when (not (zero? x))
    (csi-command! abs comm))))

(defn move-cursor-delta-y!
  [y]
  (let [abs  (Math/abs y)
        comm (if (> 0 y) \A \B)]
  (when (not (zero? y))
    (csi-command! abs comm))))

(defn move-cursor-delta!
  [x y]
  (move-cursor-delta-x! x)
  (move-cursor-delta-y! y))

(defn save-cursor-pos!    [] (csi-command! "s"))
(defn restore-cursor-pos! [] (csi-command! "u"))

; Erase
;------------------------------------------------------------------------------
(defn erase-line-to-right!       [] (csi-command! "K"))
(defn erase-line-to-left!        [] (csi-command! "1K"))
(defn erase-line-all!            [] (csi-command! "2K"))
(defn erase-screen-to-end!       [] (csi-command! "J"))
(defn erase-screen-to-beginning! [] (csi-command! "1J"))
(defn erase-screen!              [] (csi-command! "2J"))
(defn erase-screen-scroll!       [] (csi-command! "3J"))

; Cursor
;------------------------------------------------------------------------------
(defn hide-cursor! [] (csi-command! "?25l"))
(defn show-cursor! [] (csi-command! "?12l" csi-lead "?25h"))

; Colors and style
;------------------------------------------------------------------------------
(def color-pos
  {:fg \3
   :bg \4})

(def colors
  {:black   \0
   :red     \1
   :green   \2
   :yellow  \3
   :blue    \4
   :magenta \5
   :cyan    \6
   :white   \7
   :default \9})

(def styles
  {:off       \0
   :bold      \1
   :underline \4
   :blink     \5
   :reverse   \7
   :concealed \8})

(defn colorsequence
  [pos color]
  (apply str csi-lead (pos color-pos) (color colors) "m"))
(defn set-fg-color!   [color] (csi-command! (:fg color-pos) (color colors) "m"))
(defn set-bg-color!   [color] (csi-command! (:bg color-pos) (color colors) "m"))
(defn set-fgbg-color! [color] (csi-command! (:fg color-pos) (color colors) ";"
                                            (:bg color-pos) (color colors) "m"))
(defn set-style! [style] (csi-command! (style styles) "m"))

(defn wrap-color
  "Wraps string with chosen color and default color escape codes."
  [text pos color]
  (str (colorsequence pos color) text (colorsequence pos :default)))

; Query size and cursor position
;------------------------------------------------------------------------------

(defn query-cursor-pos!
  "Queues the cursor position but returns nothing. The answer must be taken
  from stdin. This is the control sequence: ESC [ y ; x R
  NOTE: the answer of the `query-size!` call and this are the same, you
  have to remember what you asked for."
  []
  (csi-command! "6n"))
(defn query-size!
  "Queues the size of the screen but returns nothing. The answer must be taken
  from stdin. This is the control sequence: ESC [ y ; x R
  NOTE: the answer of the `query-cursor-pos!` call and this are the same, you
  have to remember what you asked for."
  []
  (save-cursor-pos!)
  (move-cursor! 5000 5000)
  (query-cursor-pos!)
  (restore-cursor-pos!))


; Answer processing
;------------------------------------------------------------------------------
(def cursor-location-re #"\033\[(\d+);(\d+)R")
(def status-report-re   #"\033\[0n")

(defn parse-cursor-loc-report
  [report]
  (some->> report
           (re-seq cursor-location-re)
           first
           rest
           (map #(Integer/parseInt %))
           (zipmap [:y :x])))
(def parse-size-report parse-cursor-loc-report)
(comment (parse-cursor-loc-report "\033[12;23R")) ; Result: {:y 12, :x 23}



; -> Need something to differentiate between cursor position report and screen
; size report because the answer is the same. It's handled with this call
; queue.
; TODO: leave all this stuff commented, and only use the report as a screen
; size report for the moment. Will put it better when this is packaged as a lib
(comment
  (defonce ^:private call-report-queue (atom []))
  ; TODO CHANGE UNTIL IT WORKS LIKE A FIFO!!
  (defn shift-call-report-queue!
    []
    (let [queue @call-report-queue]
      (reset! call-report-queue (vec (rest queue)))
      (first queue)))
  (defn push-call-report-queue!
    [last-call]
    (swap! call-report-queue #(conj % last-call)))

  (defn query-cursor-pos-lock!
    "Queries cursor position while locking the call queue. Call queue ensures the
    query list is in order."
    []
    (locking call-report-queue
      (query-cursor-pos!)
      (push-call-report-queue! :pos)))

  (defn query-cursor-pos-lock!
    "Queries screen size while locking the call queue. Call queue ensures the
    query list is in order."
    []
    (locking call-report-queue
      (query-size!)
      (push-call-report-queue! :size)))

  (defn identify-report!
    (locking call-report-queue
      (shift-call-report-queue!))))

