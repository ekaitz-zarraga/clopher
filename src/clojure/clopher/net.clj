(ns clopher.net
  (:require [clojure.java.io :as io]
            [clopher.definitions :as defs]
            [clojure.string :refer [split join]])
  (:import [java.io StringWriter File]
           [java.net Socket InetSocketAddress]))
; TODO:
; - Make the request have the Gopher meaning
; - Error handling
; - Wait until socket is closed: (.isClosed socket)

(defn- ->socket
  ([host port]
   (->socket host port 10000))
  ([host port timeout]
  (doto (Socket.)
        (.setSoTimeout timeout)
        (.connect (InetSocketAddress. host port) timeout))))

(defn- get-extension
  "Get's the extension of the given path. If no extension returns nil."
  [path]
  (->> (split path #"\.")
       rest
       (join ".")
       (#(if (= 0 (count %))
           nil
           (str "." %)))))

(defn- ->tempfile
  "Creates a temporary file with the provided extension. If extension is nil
  it adds `.tmp`."
  [extension]
  (doto
    (. File createTempFile "clopher" extension)
    .deleteOnExit))


; TODO wait for the `.` instead of reading everything like crazy
(defn- send-text-request
  [host port body]
  (with-open [sock (->socket host port)
              writer (io/writer sock)
              reader (io/reader sock)
              response (StringWriter.)]
    (.append writer body)
    (.flush writer)
    (io/copy reader response)
    (str response)))
(comment (send-text-request "tilde.team" 70 (str "~giacomo" defs/CR-LF)))

(defn download-file-to
  [host port srcpath destpath]
  (with-open [sock   (->socket host port)
              writer (io/writer sock)
              reader (io/reader sock)]
    (.append writer (str srcpath defs/CR-LF))
    (.flush writer)
    (io/copy reader
             (io/output-stream
               (or (io/file destpath)
                   (->tempfile (get-extension srcpath)))))))
(comment (download-file-to "tilde.team" 70 "/wiki/administration.md" "hola.txt"))

(defn text-transaction
  [host port query]
  (send-text-request host port (str query defs/CR-LF)))
(def menu-transaction     text-transaction) ;type 1
(def textfile-transaction text-transaction) ;type 0
(defn fulltext-search-transaction
  [host port selector search]
  (send-text-request host port (str selector defs/Tab search defs/CR-LF))) ;type 7
