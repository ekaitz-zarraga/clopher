(ns clopher.definitions)

(def CR-LF "\r\n")
(def Tab \tab)
(def NUL \ )
(def Lastline (str "." CR-LF))

(def Types
"0   Item is a file
 1   Item is a directory
 2   Item is a CSO phone-book server
 3   Error
 4   Item is a BinHexed Macintosh file.
 5   Item is DOS binary archive of some sort.
 6   Item is a UNIX uuencoded file.
 7   Item is an Index-Search server.
 8   Item points to a text-based telnet session.
 9   Item is a binary file!
 +   Item is a redundant server
 T   Item points to a text-based tn3270 session.
 g   Item is a GIF format graphics file.
 I   Item is some kind of image file.  Client decides how to display.
-- Non canonical
 h   Item is an HTML file
 i   Item is an informational message
 s   Item is a sound file
")
