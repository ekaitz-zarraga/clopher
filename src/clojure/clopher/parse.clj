(ns clopher.parse
  (:require [clojure.string :refer [split]]
            [clopher.definitions :as defs]))

(def parse-port #(Integer/parseInt %))

(defn parse-menu
  [content]
  ; TODO
  (->> (split content (re-pattern defs/CR-LF))
       (map (juxt first identity))))
